// import Layout from './layoutTela';
// import LayoutGrade from './layoutGrade';
 //import Components from './components';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import SegundaTela from "./src/segundaTela";
import TelaInicial from "./src/telainicial"
import Menu from "./src/components/menu";


const Stack = createStackNavigator();

export default function App() {
    return (
        <NavigationContainer>
            <Stack.Navigator>
            <Stack.Screen name="Menu" component={Menu} />
                <Stack.Screen name="TelaInicial" component={TelaInicial} />
                <Stack.Screen name="SegundaTela" component={SegundaTela} />
            </Stack.Navigator>
        </NavigationContainer>
    );
}
