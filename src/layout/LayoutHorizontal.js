import { StatusBar } from "expo-status-bar";
import { StyleSheet, Text, View, ScrollView } from "react-native";
import Layout from "../LayoutTela";

export default function LayoutHorizontal() {
return (
    <View style={styles.container}>

        <ScrollView horizontal={true}></ScrollView>
    
    <View style={styles.box1}></View>
    <View style={styles.box2}></View>
    <View style={styles.box3}></View>
    <View style={styles.box1}></View>
    <View style={styles.box2}></View>
    <View style={styles.box3}></View>
    
    </View>
    );
}

const styles = StyleSheet.create({
container: {
    flex: 1,
    flexDirection: "row",
},
    box1: {
    width: 50,
    height: 50,
    backgroundColor: "blue",
},
    box2: {
    width: 50,
    height: 50,
    backgroundColor: "purple",
},
    box3: {
    width: 50,
    height: 50,
    backgroundColor: "pink",
},

});
